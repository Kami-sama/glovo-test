package com.glovoapp.backender.service.impl;

import com.glovoapp.backender.domain.Statistic;
import com.glovoapp.backender.model.Courier;
import com.glovoapp.backender.model.Order;
import com.glovoapp.backender.service.CourierService;
import com.glovoapp.backender.service.OrderService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class StatisticServiceImplTest {

    private StatisticServiceImpl statisticService;
    private OrderService orderService;
    private CourierService courierService;

    @BeforeEach
    void setUp() {
        mockWith(0, 0, 0.0);
    }

    @Test
    void returnStatisticEmptyStatics() {
        Statistic statistic = statisticService.getStatistic();

        assertThat(statistic).extracting(Statistic::getOrders,
                Statistic::getCouriers,
                Statistic::getPercentageNonFoodOrders)
                .containsExactly(0, 0, 0.0);
    }

    @Test
    void returnStatistics() {
        mockWith(2, 1, 0.0);

        Statistic statistic = statisticService.getStatistic();

        assertThat(statistic).extracting(Statistic::getOrders,
                Statistic::getCouriers,
                Statistic::getPercentageNonFoodOrders)
                .containsExactly(2, 1, 0.0);

    }

    @Test
    void returnFullStatisticWithNotZeroPercentage() {
        mockWith(2, 1, 0.5);

        Statistic statistic = statisticService.getStatistic();

        assertThat(statistic).extracting(Statistic::getOrders,
                Statistic::getCouriers,
                Statistic::getPercentageNonFoodOrders)
                .containsExactly(2, 1, 0.5);
    }

    private void mockWith(int orderNumbers, int courierNumbers, double percentageNonFoodOrders) {
        orderService = new OrderService() {

            @Override
            public List<Order> findAll() {
                return null;
            }

            @Override
            public List<Order> findForCourier(String courierId) {
                return null;
            }

            @Override
            public int getNumberOfAllOrders() {
                return orderNumbers;
            }

            @Override
            public double getNonFoodPercentageOrders() {
                return percentageNonFoodOrders;
            }
        };

        courierService = new CourierService() {
            @Override
            public Courier findById(String courierId) {
                return null;
            }

            @Override
            public int getNumberOfAllCouriers() {
                return courierNumbers;
            }
        };


        statisticService = new StatisticServiceImpl(orderService, courierService);
    }
}