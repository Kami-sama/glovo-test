package com.glovoapp.backender.service.impl;

import com.glovoapp.backender.domain.distance.DistanceSlotResolver;
import com.glovoapp.backender.domain.prioritize.VipFoodPrioritizeHelper;
import com.glovoapp.backender.model.Order;
import com.glovoapp.backender.repository.CourierRepository;
import com.glovoapp.backender.repository.OrderRepository;
import com.glovoapp.backender.service.CourierService;
import com.glovoapp.backender.service.OrderService;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


class OrderServiceImplTest {

    private static final String COURIER_ID_WITH_BOX_ON_MOTORCYCLE = "courier-1";
    private static final String COURIER_ID_WITHOUT_BOX_ON_MOTORCYCLE = "courier-2";
    private static final String COURIER_ID_WITH_BOX_ON_BICYCLE = "courier-3";
    private static final String COURIER_ID_WITHOUT_BOX_ON_BICYCLE = "courier-4";
    private static final String COURIER_ID_WITHOUT_BOX_ON_BICYCLE_WHICH_CANNOT_REACH_DISTANCE_LIMIT = "courier-5";
    private static final String[] BOX_REQUIRED_WORDS = {"pizza", "cake", "flamingo"};
    private static final String[] EMPTY_BOX_WORDS = {};
    private static final double NON_ENGINE_DISTANCE_LIMIT = 5.0;

    private final OrderRepository orderRepository = new OrderRepository();
    private final CourierService courierRepository = new CourierServiceImpl(new CourierRepository());
    private final DistanceSlotResolver slotResolver = new DistanceSlotResolver(0.5, 1, 100);
    private final VipFoodPrioritizeHelper prioritizeHelper = new VipFoodPrioritizeHelper(true, true);

    private OrderService service = new OrderServiceImpl(orderRepository, courierRepository, slotResolver, prioritizeHelper,
            BOX_REQUIRED_WORDS, NON_ENGINE_DISTANCE_LIMIT);

    @Test
    void findOrdersForCourierWithoutBox() {
        List<Order> orders = service.findForCourier(COURIER_ID_WITHOUT_BOX_ON_MOTORCYCLE);

        assertThat(orders).extracting(Order::getId).containsOnly("order-4", "order-3", "order-6", "order-5");
    }

    @Test
    void findOrdersForCourierWithoutBoxAndWithEmptyRequiredBoxWord() {
        service = new OrderServiceImpl(orderRepository, courierRepository, slotResolver, prioritizeHelper,
                EMPTY_BOX_WORDS, NON_ENGINE_DISTANCE_LIMIT);

        List<Order> orders = service.findForCourier(COURIER_ID_WITHOUT_BOX_ON_MOTORCYCLE);

        assertThat(orders).hasSize(10);
    }

    public static void main(String[] args) {
        new OrderServiceImplTest().findOrdersForCourierWithoutBoxAndWithEmptyRequiredBoxWord();
    }

    @Test
    void findOrdersForCourierWithBox() {
        List<Order> orders = service.findForCourier(COURIER_ID_WITH_BOX_ON_MOTORCYCLE);

        assertThat(orders).hasSize(10)
                .extracting(Order::getId)
                .containsExactly("order-0", "order-1", //0-500 meters
                        "order-7", "order-9", "order-8",//501-1000 meters
                        "order-4", "order-2", "order-3", "order-6", "order-5"); //more than 1000 meters
    }

    @Test
    void findOrdersForCourierIfHeOnBicycleWithBox() {
        List<Order> orders = service.findForCourier(COURIER_ID_WITH_BOX_ON_BICYCLE);

        assertThat(orders).extracting(Order::getId).containsExactly("order-7", "order-9", "order-1", "order-8",
                "order-0", "order-2", "order-6");
    }

    @Test
    void notFindAnyOrdersForCourierIfHeOnBicycleWithoutBox() {
        List<Order> orders = service.findForCourier(COURIER_ID_WITHOUT_BOX_ON_BICYCLE_WHICH_CANNOT_REACH_DISTANCE_LIMIT);

        assertThat(orders).isEmpty();
    }

    @Test
    void findOrderForCourierIfHeOnBicycleWithoutBox() {
        List<Order> orders = service.findForCourier(COURIER_ID_WITHOUT_BOX_ON_BICYCLE);

        assertThat(orders).extracting(Order::getId).containsExactly("order-6");

    }


    @Test
    void returnPercentageOfNonFoodOrders() {
        System.out.println(service.getNonFoodPercentageOrders());
        System.out.println(service.getNumberOfAllOrders());
    }
}