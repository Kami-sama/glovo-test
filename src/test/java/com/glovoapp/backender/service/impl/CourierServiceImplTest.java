package com.glovoapp.backender.service.impl;

import com.glovoapp.backender.domain.Vehicle;
import com.glovoapp.backender.model.Courier;
import com.glovoapp.backender.model.Location;
import com.glovoapp.backender.repository.CourierRepository;
import com.glovoapp.backender.service.CourierService;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CourierServiceImplTest {

    private CourierService service = new CourierServiceImpl(new CourierRepository());

    @Test
    void findCourierById() {
        Courier courier = service.findById("courier-1");

        assertThat(courier).extracting(Courier::getId, Courier::getBox, Courier::getLocation, Courier::getName, Courier::getVehicle)
                .containsExactly("courier-1", true, new Location(41.3965463, 2.1963997), "Manolo Escobar", Vehicle.MOTORCYCLE);
    }

    @Test
    void didNotFindCourierWithNonexistentId() {
        Courier courier = service.findById("test123");

        assertThat(courier).isNull();
    }

    @Test
    void throwNpeForNullId() {
        assertThrows(NullPointerException.class, () -> service.findById(null));
    }
}