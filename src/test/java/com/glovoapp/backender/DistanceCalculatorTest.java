package com.glovoapp.backender;

import com.glovoapp.backender.domain.distance.DistanceCalculator;
import com.glovoapp.backender.model.Location;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DistanceCalculatorTest {
    @Test
    public void smokeTest() {
        Location francescMacia = new Location(41.3925603, 2.1418532);
        Location placaCatalunya = new Location(41.3870194, 2.1678584);

        // More or less 2km from Francesc Macia to Placa Catalunya
        assertEquals(2.0, DistanceCalculator.calculateDistance(francescMacia, placaCatalunya), 0.5);
    }

    @Test
    void vectorShouldBeMoreThan5km() {
        Location from = new Location(41.3999999, 2.1963997);
        Location to = new Location(41.3550000, 2.1963997);

        assertTrue(DistanceCalculator.calculateDistance(from, to) > 5.0);
    }
}