package com.glovoapp.backender.domain.prioritize;

import com.glovoapp.backender.model.Order;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class VipFoodPrioritizeComparatorTest {

    @ParameterizedTest
    @MethodSource("data")
    void compare(boolean vipEnable, boolean foodEnable, boolean o1Vip, boolean o1Food, boolean o2Vip, boolean o2Food, int result) {
        VipFoodPrioritizeComparator comparator = new VipFoodPrioritizeComparator(vipEnable, foodEnable);
        int compare = comparator.compare(order(o1Vip, o1Food), order(o2Vip, o2Food));

        assertEquals(result, compare);
    }


    static Stream<Arguments> data() {
        return Stream.of(
                arguments(true, true, true, false, false, false, -1),
                arguments(true, true, false, false, true, false, 1),
                arguments(true, true, true, false, true, false, 0),
                arguments(true, true, false, false, false, false, 0),
                arguments(true, true, false, true, false, false, -1),
                arguments(true, true, false, false, false, true, 1),
                arguments(false, true, false, false, false, false, 0),
                arguments(false, true, false, true, false, false, -1),
                arguments(false, true, false, false, false, true, 1),
                arguments(false, true, false, true, false, true, 0),
                arguments(false, true, false, false, false, false, 0),
                arguments(false, false, false, false, false, false, 0),
                arguments(false, false, true, false, false, false, 0),
                arguments(false, false, false, false, false, true, 0),
                arguments(false, false, false, false, false, true, 0),
                arguments(true, true, true, true, true, true, 0)

        );
    }

    private Order order(boolean vip, boolean food) {
        return Order.builder().vip(vip).food(food).build();
    }
}