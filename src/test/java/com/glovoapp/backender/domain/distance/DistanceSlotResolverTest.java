package com.glovoapp.backender.domain.distance;

import com.glovoapp.backender.model.Location;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


class DistanceSlotResolverTest {

    private DistanceSlotResolver resolver = new DistanceSlotResolver(0.5, 1, 100);

    @Test
    void returnFirstSlotAround111Meters() {
        DistanceSlot slot = resolver.resolve(new Location(42.30, 2.0),
                new Location(42.301, 2.0));

        assertThat(slot).isEqualTo(DistanceSlot.FIRST_SLOT);
    }

    @Test
    void returnSecondSlotAround890Meters() {
        DistanceSlot slot = resolver.resolve(new Location(42.30, 2.0),
                new Location(42.308, 2.0));

        assertThat(slot).isEqualTo(DistanceSlot.SECOND_SLOT);
    }

    @Test
    void returnOtherSlotAround2001Meters() {
        DistanceSlot slot = resolver.resolve(new Location(42.30, 2.0),
                new Location(42.318, 2.0));

        assertThat(slot).isEqualTo(DistanceSlot.OTHER_SLOT);
    }

    @Test
    void returnOutOfSlotAround2001Meters() {
        DistanceSlot slot = resolver.resolve(new Location(42.30, 2.0),
                new Location(41.018, 2.0));

        assertThat(slot).isEqualTo(DistanceSlot.OUT_OF_SLOTS);
    }

    @Test
    void expectedNpeWithNullFromLocation() {
        Assertions.assertThrows(NullPointerException.class,
                () -> resolver.resolve(null, new Location(41.018, 2.0)));
    }

    @Test
    void expectedNpeWithNullToLocation() {
        Assertions.assertThrows(NullPointerException.class,
                () -> resolver.resolve(new Location(42.30, 2.0), null));

    }
}