package com.glovoapp.backender.domain.distance;

import com.glovoapp.backender.model.Location;
import com.glovoapp.backender.model.Order;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class DistanceComparatorTest {

    private Location location = new Location(42.301, 2.0);
    private DistanceComparator comparator = new DistanceComparator(location);

    @Test
    void firstOrderLocationClosely() {
        int compare = comparator.compare(
                order(new Location(42.309, 2.0)),
                order(new Location(42.304, 2.0)));

        assertTrue(compare > 0);
    }

    @Test
    void secondOrderLocationClosely() {
        int compare = comparator.compare(
                order(new Location(42.304, 2.0)),
                order(new Location(42.309, 2.0)));

        assertTrue(compare < 0);
    }

    @Test
    void bothOrderOnTheSameDistance() {
        int compare = comparator.compare(
                order(new Location(42.304, 2.0)),
                order(new Location(42.304, 2.0)));

        assertTrue(compare == 0);
    }

    private Order order(Location l) {
        return Order.builder().pickup(l).build();
    }

}