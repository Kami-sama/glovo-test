package com.glovoapp.backender;

import com.glovoapp.backender.domain.OrderPredicate;
import com.glovoapp.backender.domain.Vehicle;
import com.glovoapp.backender.model.Courier;
import com.glovoapp.backender.model.Location;
import com.glovoapp.backender.model.Order;
import org.junit.jupiter.api.Test;

import java.util.function.BiPredicate;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class OrderPredicateTest {

    private static final String[] BOX_REQUIRED_WORDS = {"pizza", "cake", "flamingo"};
    private static final double NON_ENGINE_DISTANCE_LIMIT = 5.0;
    private static final Location PICKUP_LOCATION = new Location(41.3965463, 2.1963997);
    private static final String NO_BOX_REQUIRED_DESCRIPTION = "NO box required description";

    private BiPredicate<Order, Courier> predicate = new OrderPredicate(BOX_REQUIRED_WORDS, NON_ENGINE_DISTANCE_LIMIT);

    @Test
    void trueIfHasEngineAndEquippedWithBox() {
        assertTrue(predicate.test(createOrder(), createCourier(true, Vehicle.MOTORCYCLE)));
    }

    @Test
    void trueIfHasNotEngineAndEquippedWithBox() {
        assertTrue(predicate.test(createOrder(), createCourier(true, Vehicle.BICYCLE)));
    }

    @Test
    void trueIfHasEngineAndEquippedWithoutBox() {
        assertTrue(predicate.test(createOrder(PICKUP_LOCATION, NO_BOX_REQUIRED_DESCRIPTION),
                createCourier(false, Vehicle.MOTORCYCLE)));
    }

    @Test
    void trueIfHasNotEngineAndEquippedWithoutBox() {
        Order order = createOrder(PICKUP_LOCATION, NO_BOX_REQUIRED_DESCRIPTION);
        assertTrue(predicate.test(order, createCourier(false, Vehicle.BICYCLE)));

        assertTrue(predicate.test(createOrder(PICKUP_LOCATION, "I want Cheesecake"), createCourier(false, Vehicle.BICYCLE)));
        assertTrue(predicate.test(createOrder(PICKUP_LOCATION, "I don't \n want Cheesecake"), createCourier(false, Vehicle.BICYCLE)));
        assertTrue(predicate.test(createOrder(PICKUP_LOCATION, "I want a berry"), createCourier(false, Vehicle.BICYCLE)));
    }

    @Test
    void falseIfHasNotEngineAndEquippedWithBox() {
        Location location = new Location(41.0065463, 2.1963997);
        assertFalse(predicate.test(createOrder(location, "pizza"), createCourier(true, Vehicle.BICYCLE)));
    }

    @Test
    void falseIfHasEngineAndEquippedWithoutBox() {
        assertFalse(predicate.test(createOrder(), createCourier(false, Vehicle.ELECTRIC_SCOOTER)));
    }

    @Test
    void falseIfHasNotEngineAndEquippedWithoutBox() {
        assertFalse(predicate.test(createOrder(), createCourier(false, Vehicle.BICYCLE)));
    }


    private Order createOrder() {
        return createOrder(PICKUP_LOCATION, "I want a pizza cut into very small slices");
    }

    private Order createOrder(Location pickupLocation, String description) {
        return Order.builder()
                .id("order-1")
                .description(description)
                .food(true)
                .vip(false)
                .pickup(pickupLocation)
                .delivery(new Location(41.407834, 2.1675979))
                .build();
    }

    private Courier createCourier(boolean box, Vehicle vehicle) {
        return Courier.builder().id("courier-1")
                .box(box)
                .name("Manolo Escobar")
                .vehicle(vehicle)
                .location(new Location(41.3965463, 2.1963997))
                .build();
    }

}