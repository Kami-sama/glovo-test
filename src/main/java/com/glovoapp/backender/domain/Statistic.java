package com.glovoapp.backender.domain;

import lombok.Value;

@Value
public class Statistic {
    private int orders;
    private int couriers;
    private Double percentageNonFoodOrders;
}
