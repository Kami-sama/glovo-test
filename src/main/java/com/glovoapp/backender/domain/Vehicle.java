package com.glovoapp.backender.domain;

public enum Vehicle {
    MOTORCYCLE(true), BICYCLE(false), ELECTRIC_SCOOTER(true);

    private boolean hasEngine;

    Vehicle(boolean hasEngine) {
        this.hasEngine = hasEngine;
    }

    public boolean isHasEngine() {
        return hasEngine;
    }
}
