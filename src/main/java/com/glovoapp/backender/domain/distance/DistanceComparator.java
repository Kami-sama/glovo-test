package com.glovoapp.backender.domain.distance;

import com.glovoapp.backender.model.Location;
import com.glovoapp.backender.model.Order;

import java.util.Comparator;

public class DistanceComparator implements Comparator<Order> {

    private Location baseLocation;

    public DistanceComparator(Location baseLocation) {
        this.baseLocation = baseLocation;
    }

    @Override
    public int compare(Order o1, Order o2) {
        double d1 = DistanceCalculator.calculateDistance(baseLocation, o1.getPickup());
        double d2 = DistanceCalculator.calculateDistance(baseLocation, o2.getPickup());
        return Double.compare(d1, d2);
    }
}
