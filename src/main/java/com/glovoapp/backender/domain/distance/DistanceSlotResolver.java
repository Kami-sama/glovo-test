package com.glovoapp.backender.domain.distance;

import com.glovoapp.backender.model.Location;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import static java.lang.String.format;

@Component
public class DistanceSlotResolver {

    private final double slotFirstLimit;
    private final double slotSecondLimit;
    private final double slotOtherLimit;

    public DistanceSlotResolver(@Value("${glovo.slot.first.limit:0.5}") double slotFirstLimit,
                                @Value("${glovo.slot.second.limit:1}") double slotSecondLimit,
                                @Value("${glovo.slot.other.limit:100}") double slotOtherLimit) {
        this.slotFirstLimit = slotFirstLimit;
        this.slotSecondLimit = slotSecondLimit;
        this.slotOtherLimit = slotOtherLimit;
    }


    public DistanceSlot resolve(Location from, Location to) {
        if (from == null || to == null)
            throw new NullPointerException(format("From:%s or To:%s cannot be null", from, to));

        double d = DistanceCalculator.calculateDistance(from, to);
        if (d <= slotFirstLimit)
            return DistanceSlot.FIRST_SLOT;
        if (d > slotFirstLimit && d <= slotSecondLimit)
            return DistanceSlot.SECOND_SLOT;
        if (d > slotSecondLimit && d <= slotOtherLimit)
            return DistanceSlot.OTHER_SLOT;

        return DistanceSlot.OUT_OF_SLOTS;

    }

}
