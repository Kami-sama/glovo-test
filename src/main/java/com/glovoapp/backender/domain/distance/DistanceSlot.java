package com.glovoapp.backender.domain.distance;

public enum DistanceSlot {
    FIRST_SLOT, SECOND_SLOT, OTHER_SLOT, OUT_OF_SLOTS
}
