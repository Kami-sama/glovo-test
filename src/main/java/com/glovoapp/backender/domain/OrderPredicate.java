package com.glovoapp.backender.domain;

import com.glovoapp.backender.domain.distance.DistanceCalculator;
import com.glovoapp.backender.model.Courier;
import com.glovoapp.backender.model.Location;
import com.glovoapp.backender.model.Order;

import java.util.function.BiPredicate;
import java.util.regex.Pattern;

import static java.lang.String.format;

public class OrderPredicate implements BiPredicate<Order, Courier> {

    private static final String PATTER_WORD_REGEXP = ".*\\b(%s)\\b.*";
    private String[] boxRequiredWords;
    private double nonEngineDistanceLimit;

    private Pattern pattern;

    public OrderPredicate(String[] boxRequiredWords, double nonEngineDistanceLimit) {
        this.boxRequiredWords = boxRequiredWords;
        this.nonEngineDistanceLimit = nonEngineDistanceLimit;

        String regex = "";
        if (boxRequiredWords != null && boxRequiredWords.length > 0)
            regex = format(PATTER_WORD_REGEXP, String.join("|", boxRequiredWords));

        pattern = Pattern.compile(regex);
    }

    @Override
    public boolean test(Order order, Courier courier) {
        return allowOrderByVehicleAndLocation(order, courier.getVehicle(), courier.getLocation())
                && allowOrderByBox(order, courier.isEquippedBox());
    }

    private boolean allowOrderByVehicleAndLocation(Order order, Vehicle vehicle, Location location) {
        return vehicle.isHasEngine()
                || DistanceCalculator.calculateDistance(order.getPickup(), location) < nonEngineDistanceLimit;

    }

    private boolean allowOrderByBox(Order order, boolean isEquippedBox) {
        return isEquippedBox
                || boxRequiredWords == null || boxRequiredWords.length < 1
                || !pattern.matcher(order.getDescription().toLowerCase()).find();
    }
}
