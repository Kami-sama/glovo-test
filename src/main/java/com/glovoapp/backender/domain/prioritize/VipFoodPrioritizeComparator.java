package com.glovoapp.backender.domain.prioritize;

import com.glovoapp.backender.model.Order;

import java.util.Comparator;

public class VipFoodPrioritizeComparator implements Comparator<Order> {

    private final boolean vipEnable;
    private final boolean foodEnable;

    VipFoodPrioritizeComparator(boolean vipEnable, boolean foodEnable) {
        this.vipEnable = vipEnable;
        this.foodEnable = foodEnable;
    }

    @Override
    public int compare(Order o1, Order o2) {
        if (vipEnable && (o1.getVip() || o2.getVip()))
            return Boolean.compare(o2.getVip(), o1.getVip());

        if (foodEnable)
            return Boolean.compare(o2.getFood(), o1.getFood());

        return 0;
    }
}
