package com.glovoapp.backender.domain.prioritize;

import com.glovoapp.backender.domain.distance.DistanceSlot;
import com.glovoapp.backender.model.Order;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static java.util.stream.Collectors.toList;

@Component
public class VipFoodPrioritizeHelper {

    private final boolean vipEnable;
    private final boolean foodEnable;

    public VipFoodPrioritizeHelper(@Value("${glovo.prioritizing.vip.enable:false}") boolean vipEnable,
                                   @Value("${glovo.prioritizing.food.enable:false}") boolean foodEnable) {
        this.vipEnable = vipEnable;
        this.foodEnable = foodEnable;
    }

    /**
     * Make prioritization based on vip and food selectors
     *
     * @param sortedSlots map should be sorted by slot priority, lists in map should be sorted.
     *                    It guarantees sorted and prioritized result.
     */
    public List<Order> prioritizeSlotsToResultList(Map<DistanceSlot, List<Order>> sortedSlots) {
        sortedSlots.remove(DistanceSlot.OUT_OF_SLOTS);

        return sortedSlots.values().stream()
                .map(mapToPrioritizedList())
                .flatMap(Collection::stream)
                .collect(toList());
    }

    private Function<List<Order>, List<Order>> mapToPrioritizedList() {
        return orders -> orders.stream()
                .sorted(new VipFoodPrioritizeComparator(vipEnable, foodEnable))
                .collect(toList());
    }
}
