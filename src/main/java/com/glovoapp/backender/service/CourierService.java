package com.glovoapp.backender.service;

import com.glovoapp.backender.model.Courier;

public interface CourierService {
    Courier findById(String courierId);

    int getNumberOfAllCouriers();
}
