package com.glovoapp.backender.service;

import com.glovoapp.backender.domain.Statistic;

public interface StatisticService {
    Statistic getStatistic();
}
