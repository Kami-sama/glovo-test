package com.glovoapp.backender.service;

import com.glovoapp.backender.model.Order;

import java.util.List;

public interface OrderService {
    List<Order> findAll();

    List<Order> findForCourier(String courierId);

    int getNumberOfAllOrders();

    double getNonFoodPercentageOrders();
}
