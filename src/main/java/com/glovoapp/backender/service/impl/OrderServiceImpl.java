package com.glovoapp.backender.service.impl;

import com.glovoapp.backender.domain.OrderPredicate;
import com.glovoapp.backender.domain.distance.DistanceComparator;
import com.glovoapp.backender.domain.distance.DistanceSlot;
import com.glovoapp.backender.domain.distance.DistanceSlotResolver;
import com.glovoapp.backender.domain.prioritize.VipFoodPrioritizeHelper;
import com.glovoapp.backender.model.Courier;
import com.glovoapp.backender.model.Order;
import com.glovoapp.backender.repository.OrderRepository;
import com.glovoapp.backender.service.CourierService;
import com.glovoapp.backender.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

@Service
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final CourierService courierService;
    private final DistanceSlotResolver slotResolver;
    private final VipFoodPrioritizeHelper prioritizeHelper;
    private final String[] boxRequiredWords;
    private final double nonEngineDistanceLimit;

    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository, CourierService courierService,
                            DistanceSlotResolver slotResolver, VipFoodPrioritizeHelper prioritizeHelper,
                            @Value("${glovo.box.required.words:}") String[] boxRequiredWords,
                            @Value("${glovo.nonengine.distance.limit:5.0}") double nonEngineDistanceLimit) {
        this.orderRepository = orderRepository;
        this.courierService = courierService;
        this.slotResolver = slotResolver;
        this.prioritizeHelper = prioritizeHelper;
        this.boxRequiredWords = boxRequiredWords;
        this.nonEngineDistanceLimit = nonEngineDistanceLimit;
    }

    @Override
    public List<Order> findAll() {
        return orderRepository.findAll();
    }

    @Override
    public List<Order> findForCourier(String courierId) {
        Courier courier = courierService.findById(courierId);
        List<Order> orders = findAll();

        if (orders == null) return emptyList();

        OrderPredicate orderPredicate = new OrderPredicate(boxRequiredWords, nonEngineDistanceLimit);

        Map<DistanceSlot, List<Order>> slotsMap = orders.stream()
                .filter(o -> orderPredicate.test(o, courier))
                .sorted(new DistanceComparator(courier.getLocation()))
                .collect(groupingBy(o -> slotResolver.resolve(courier.getLocation(), o.getPickup()), TreeMap::new, toList()));

        return prioritizeHelper.prioritizeSlotsToResultList(slotsMap);
    }

    @Override
    public int getNumberOfAllOrders() {
        return findAll().size();
    }

    @Override
    public double getNonFoodPercentageOrders() {
        long countOfNonFoodOrder = findAll().stream()
                .filter(o -> !o.getFood())
                .count();

        return (double) countOfNonFoodOrder / getNumberOfAllOrders();
    }


}
