package com.glovoapp.backender.service.impl;

import com.glovoapp.backender.domain.Statistic;
import com.glovoapp.backender.service.CourierService;
import com.glovoapp.backender.service.OrderService;
import com.glovoapp.backender.service.StatisticService;
import org.springframework.stereotype.Service;

@Service
public class StatisticServiceImpl implements StatisticService {

    private CourierService courierService;
    private OrderService orderService;

    public StatisticServiceImpl(OrderService orderService, CourierService courierService) {
        this.orderService = orderService;
        this.courierService = courierService;
    }

    @Override
    public Statistic getStatistic() {
        int ordersNumber = orderService.getNumberOfAllOrders();
        int couriersNumber = courierService.getNumberOfAllCouriers();
        double nonFoodPercentageOrders = orderService.getNonFoodPercentageOrders();
        return new Statistic(ordersNumber, couriersNumber, nonFoodPercentageOrders);
    }
}
