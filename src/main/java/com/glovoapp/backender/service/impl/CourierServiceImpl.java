package com.glovoapp.backender.service.impl;

import com.glovoapp.backender.model.Courier;
import com.glovoapp.backender.repository.CourierRepository;
import com.glovoapp.backender.service.CourierService;
import org.springframework.stereotype.Service;

@Service
public class CourierServiceImpl implements CourierService {

    private final CourierRepository repository;

    public CourierServiceImpl(CourierRepository repository) {
        this.repository = repository;
    }

    @Override
    public Courier findById(String courierId) {
        return repository.findById(courierId);
    }

    @Override
    public int getNumberOfAllCouriers() {
        return repository.findAll().size();
    }
}
