package com.glovoapp.backender.controller;


import com.glovoapp.backender.domain.Statistic;
import com.glovoapp.backender.service.StatisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


//Add a /stats endpoint with:
// - Number of orders
// - Number of couriers
// - Percentage of non food orders

@RestController
public class StatisticController {

    @Autowired
    private StatisticService service;

    @GetMapping("/stats")
    public Statistic stats() {
        return service.getStatistic();
    }

}
