package com.glovoapp.backender.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

    @GetMapping("/")
    public String root(@Value("${glovo.welcome.message:Hello}") String welcomeMessage) {
        return welcomeMessage;
    }
}
