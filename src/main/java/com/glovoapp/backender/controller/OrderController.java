package com.glovoapp.backender.controller;

import com.glovoapp.backender.dto.OrderDto;
import com.glovoapp.backender.model.Order;
import com.glovoapp.backender.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping("/orders")
    public List<OrderDto> orders() {
        return convertToOrderDto(orderService.findAll());
    }

    @GetMapping("/orders/{courierId}")
    public List<OrderDto> ordersForCourier(@PathVariable String courierId) {
        return convertToOrderDto(orderService.findForCourier(courierId));
    }

    private List<OrderDto> convertToOrderDto(List<Order> orders) {
        return orders.stream()
                .map(order -> new OrderDto(order.getId(), order.getDescription()))
                .collect(Collectors.toList());
    }
}
