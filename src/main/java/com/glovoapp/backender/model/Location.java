package com.glovoapp.backender.model;

import lombok.Value;

@Value
public class Location {
    private Double lat;
    private Double lon;
}
