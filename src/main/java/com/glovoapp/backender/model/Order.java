package com.glovoapp.backender.model;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Order {
    private String id;
    private String description;
    private Boolean food;
    private Boolean vip;
    private Location pickup;
    private Location delivery;
}
