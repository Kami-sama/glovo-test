package com.glovoapp.backender.model;

import com.glovoapp.backender.domain.Vehicle;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Courier {
    private String id;
    private String name;
    private Boolean box;
    private Vehicle vehicle;
    private Location location;

    public boolean isEquippedBox() {
        return box;
    }
}
