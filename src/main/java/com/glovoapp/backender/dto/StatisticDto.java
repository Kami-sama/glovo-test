package com.glovoapp.backender.dto;

public class StatisticDto {
    private Long orders;
    private Long couriers;
    private Double persentageNonFoodOrders;
}
