package com.glovoapp.backender.dto;

/**
 * To be used for exposing order information through the API
 */
public class OrderDto {
    private String id;
    private String description;

    public OrderDto(String id, String description) {
        this.id = id;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }
}
